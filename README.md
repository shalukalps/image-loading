Image loading and caching

git@gitlab.com:shalukalps/image-loading.git

1. Image Loading from URL
2. Staggered Grid View
3. Swipe to refresh
4. Import activity in dialog popup
5. Asynchronous Image loading
6. Memory Cache
7. Storing and retrieving from shared Preferences
8. Ripple effect clicking on card view
