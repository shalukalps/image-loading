package com.image.loading.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.image.loading.R;
import com.image.loading.configs.Constants;
import com.image.loading.helpers.ImageLoader;

/**
 * Created by kalpana on 09/11/2016.
 */

public class ViewDetailsActivity extends Activity {
    TextView username;
    TextView name;
    TextView id;
    TextView html;
    ImageView imageView;
    ProgressBar progressBar;
    String suffix;
    int position;

    public static Spanned getHtmlFromString(String source) {
        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(source);
        }

        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_details);
        username = (TextView) findViewById(R.id.username);
        name = (TextView) findViewById(R.id.name);
        id = (TextView) findViewById(R.id.id);
        html = (TextView) findViewById(R.id.html);
        imageView = (ImageView) findViewById(R.id.image);
        progressBar = (ProgressBar) findViewById(R.id.pb_loading);
        position = getIntent().getExtras().getInt("position");
        suffix = Constants.UNDER_SCORE + position;

        SharedPreferences sharedPreferences = getSharedPreferences("shared_preferences", Context.MODE_PRIVATE);

        ImageLoader imageLoader = new ImageLoader(getApplicationContext());
        imageLoader.DisplayImage(String.valueOf(sharedPreferences.getString(Constants.URL_KEY + suffix, "")), imageView, getApplicationContext());

        username.setText(getHtmlFromString(getResources().getString(R.string.user_name) + sharedPreferences.getString(Constants.USER_NAME_KEY + suffix, Constants.EMPTY_STRING)));
        name.setText(getHtmlFromString(getResources().getString(R.string.name) + sharedPreferences.getString(Constants.NAME_KEY + suffix, Constants.EMPTY_STRING)));
        id.setText(getHtmlFromString(getResources().getString(R.string.id) + sharedPreferences.getString(Constants.USER_ID_KEY + suffix, Constants.EMPTY_STRING)));
        html.setText(getHtmlFromString(getResources().getString(R.string.html) + sharedPreferences.getString(Constants.HTML_KEY + suffix, Constants.EMPTY_STRING)));
    }
}
