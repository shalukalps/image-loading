package com.image.loading.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.image.loading.R;
import com.image.loading.adapters.SolventRecyclerViewAdapter;
import com.image.loading.configs.Constants;
import com.image.loading.library.JSONParser;
import com.image.loading.utils.ItemObjects;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kalpana on 10/11/2016.
 */

public class LoadImagesActivity extends AppCompatActivity {

    private static String url = "http://www.pastebin.com/raw/wgkJgazE";
    ProgressBar progressBar;
    private StaggeredGridLayoutManager gaggeredGridLayoutManager;
    private SwipeRefreshLayout swipeContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_images);
        progressBar = (ProgressBar) findViewById(R.id.pb_loading);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        JsonTask jsonTask = new JsonTask();
        jsonTask.execute();
    }

    private void setUpSwipeRefresh(final JSONArray result) {
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                fetchTimelineAsync();
                refreshData(result);
            }
        });
    }

    public void refreshData(JSONArray result) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        int screensize = (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK);
        if (screensize == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(2, 1);
        } else if (screensize == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(3, 1);
        } else if (screensize == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            gaggeredGridLayoutManager = new StaggeredGridLayoutManager(4, 1);
        }

        recyclerView.setLayoutManager(gaggeredGridLayoutManager);

        List<ItemObjects> gaggeredList = getListItemData(result);

        SolventRecyclerViewAdapter rcAdapter = new SolventRecyclerViewAdapter(this, gaggeredList);
        recyclerView.setAdapter(rcAdapter);
    }

    public void fetchTimelineAsync() {
        swipeContainer.setRefreshing(false);
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<ItemObjects> getListItemData(JSONArray jsonArray) {
        List<ItemObjects> listViewItems = new ArrayList<>();

        SharedPreferences sharedPref = getSharedPreferences(Constants.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        String suffix = "";
        for (int i = 0; i < jsonArray.length(); i++) {
            listViewItems.add(new ItemObjects(jsonArray.optJSONObject(i).optJSONObject(Constants.USER).optString(Constants.NAME_KEY), jsonArray.optJSONObject(i).optJSONObject(Constants.URLS).optString(Constants.THUMB)));
            suffix = Constants.UNDER_SCORE + i;
            editor.putString(Constants.URL_KEY + suffix, jsonArray.optJSONObject(i).optJSONObject(Constants.URLS).optString(Constants.FULL));
            editor.putString(Constants.USER_NAME_KEY + suffix, jsonArray.optJSONObject(i).optJSONObject(Constants.USER).optString(Constants.USER_NAME_KEY));
            editor.putString(Constants.NAME_KEY + suffix, jsonArray.optJSONObject(i).optJSONObject(Constants.USER).optString(Constants.NAME_KEY));
            editor.putString(Constants.USER_ID_KEY + suffix, jsonArray.optJSONObject(i).optJSONObject(Constants.USER).optString(Constants.USER_ID_KEY));
            editor.putString(Constants.HTML_KEY + suffix, jsonArray.optJSONObject(i).optJSONObject(Constants.USER).optJSONObject(Constants.LINKS).optString(Constants.HTML_KEY));
        }

        editor.apply();

        return listViewItems;
    }

    private class JsonTask extends AsyncTask<String, String, JSONArray> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);

        }

        protected JSONArray doInBackground(String... params) {
            JSONParser jParser = new JSONParser();
            // Getting JSON from URL
            JSONArray json = jParser.getJSONFromUrl(url);
            return json;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.GONE);

            refreshData(result);
            setUpSwipeRefresh(result);
        }
    }

}
