package com.image.loading.configs;

/**
 * Created by kalpana on 10/11/2016.
 */

public class Constants {
    public static final String USER = "user";
    public static final String URLS = "urls";
    public static final String SHARED_PREFERENCES = "shared_preferences";
    public static final String FULL = "full";
    public static final String LINKS = "links";
    public static final String THUMB = "thumb";
    public static final String UNDER_SCORE = "_";
    public static final String USER_NAME_KEY = "username";
    public static final String NAME_KEY = "name";
    public static final String USER_ID_KEY = "id";
    public static final String HTML_KEY = "html";
    public static final String URL_KEY = "url";
    public static final String EMPTY_STRING = "";
}

