package com.image.loading.viewHolders;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.image.loading.R;
import com.image.loading.activity.ViewDetailsActivity;

/**
 * Created by kalpana on 08/11/2016.
 */

public class SolventViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView countryName;
    public ImageView countryPhoto;

    public SolventViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
        countryName = (TextView) itemView.findViewById(R.id.user_name);
        countryPhoto = (ImageView) itemView.findViewById(R.id.user_photo);
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(view.getContext(), "Clicked Position = " + getPosition(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(view.getContext(), ViewDetailsActivity.class);
        intent.putExtra("position", getPosition());
        view.getContext().startActivity(intent);
    }
}
