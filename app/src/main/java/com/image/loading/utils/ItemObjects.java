package com.image.loading.utils;

public class ItemObjects {
    private String name;
    private int photo;
    private String photoUrl;

    public ItemObjects(String name, String url) {
        this.name = name;
        this.photoUrl = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhoto() {
        return photo;
    }

    public void setPhoto(int photo) {
        this.photo = photo;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }
}
