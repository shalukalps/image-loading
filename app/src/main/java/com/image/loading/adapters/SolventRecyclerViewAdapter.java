package com.image.loading.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.image.loading.R;
import com.image.loading.helpers.ImageLoader;
import com.image.loading.services.DownloadImageTask;
import com.image.loading.utils.ItemObjects;
import com.image.loading.viewHolders.SolventViewHolders;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by kalpana on 08/11/2016.
 */

public class SolventRecyclerViewAdapter extends RecyclerView.Adapter<SolventViewHolders> {
    private List<ItemObjects> itemList;
    private Context context;

    public SolventRecyclerViewAdapter(Context context, List<ItemObjects> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    @Override
    public SolventViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {

        final View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.solvent_list, null);
        SolventViewHolders rcv = new SolventViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(SolventViewHolders holder, int position) {
        holder.countryName.setText(itemList.get(position).getName());

        ImageLoader imageLoader = new ImageLoader(context);
        imageLoader.DisplayImage(itemList.get(position).getPhotoUrl(), holder.countryPhoto, context);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}
